<?php

/**
 * @file
 *   Contains the node row plugin for custom output of field value.
 */

/**
 * Plugin which performs a node_view on the resulting object.
 *
 * The difference between this plugin and the core node row plugin is that it
 * can display node view modes based on value of field.
 */
class views_plugin_row_view_modes extends views_plugin_row_node_view {

  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = $this->options_form_summary_options();
    $form['instructions'] = array(
      '#type' => 'item',
      '#title' => t('Instructions'),
      '#markup' => t('The value above will be used as default if field value is not availble'),
    );
  }

  function summary_title() {
    $options = $this->options_form_summary_options();
    return check_plain($options[$this->options['view_mode']] . ', ' . $options[$this->options['first_settings']['view_mode']]);
  }

  function render($row) {
    if (!isset($keys)) {
      $keys = array_keys($this->nodes);
    }
    if (!isset($current_page)) {
      $current_page = $this->view->get_current_page();
    }
    if (!isset($items_per_page)) {
      $items_per_page = $this->view->get_items_per_page();
    }
    // Get the options for settings.
    $options = $this->options;

    // Get the node
    $node = $this->nodes[$row->{$this->field_alias}];
    // Define the node->view object
    $node->view = $this->view;
    // Check if the field field_view_mode has value
    if (isset($node->field_view_mode['und'][0]['option'])) {
      // Declare our value
      $view_node_field = $node->field_view_mode['und'][0]['option'];
    }
    if (isset($view_node_field)) {
      // Define the view mode of the value in the field
      $view_mode = $view_node_field;
    } else {
      // If the field does not have value, use the default.
      $view_mode = $options['view_mode'];
    }
    // And build the view
    $build = node_view($node, $view_mode);

    /* Some static variables that help us to identify at which result index
      are we in the current result set. */
    static $keys, $current_page, $items_per_page;
    // Give us the output
    return drupal_render($build);
  }

}
